<?php

/**
 * @file
 * Provides content language negotiation for entities through a query parameter.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language_negotiator_content_entity_all_routes\Plugin\LanguageNegotiation\LanguageNegotiationContentEntityAllRoutes;

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Adds the language switch library to the form if the language select widget
 * is active. The library will update the form action to point to the newly
 * selected language each time the language is changed in the language select
 * list. This is needed in order to establish correct language negotiation for
 * the content language.
 */
function language_negotiator_content_entity_all_routes_field_widget_language_select_form_alter(&$element, FormStateInterface $form_state, $context) {
  // Determine whether we should append the java script library, which we should
  // do only if our negotiation method has higher priority than the URL
  // negotiation method. For performance reasons we store the result of our
  // check into the form state storage.
  $language_negotiator_content_entity_all_routes = $form_state->get('language_negotiator_content_entity_all_routes');
  if (is_null($language_negotiator_content_entity_all_routes)) {
    /** @var \Drupal\language\LanguageNegotiatorInterface $language_negotiator */
    $language_negotiator = \Drupal::service('language_negotiator');
    /** @var \Drupal\language_negotiator_content_entity_all_routes\Plugin\LanguageNegotiation\LanguageNegotiationContentEntityAllRoutes $method */
    $method = $language_negotiator->getNegotiationMethodInstance(LanguageNegotiationContentEntityAllRoutes::METHOD_ID);
    $language_negotiator_content_entity_all_routes = $method->hasLowerLanguageNegotiationWeight();
    $form_state->set('language_negotiator_content_entity_all_routes', $language_negotiator_content_entity_all_routes);
  }

  if ($language_negotiator_content_entity_all_routes) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $items = $context['items'];
    $field_name = $items->getFieldDefinition()->getName();
    $entity_type = $items->getEntity()->getEntityType();
    if ($entity_type->getKey('langcode') === $field_name) {

      // If a new entity is being created, then usually its initial langcode
      // defaults to the site default language instead to the current content
      // language. To overcome this problem we set the current content language
      // as a language on the entity and exchange the default value in the
      // widget, so that it and not the site default language is preselected in
      // the form language widget element.
      $entity = $items->getEntity();
      if ($entity->isNew()) {
        $langcode = \Drupal::languageManager()
          ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
          ->getId();
        if ($entity->language()->getId() !== $langcode) {
          $entity->set($field_name, $langcode);
          if (isset($element['value']['#default_value'])) {
            $element['value']['#default_value'] = $langcode;
          }
        }
      }

      $element['value']['#attributes']['class'][] = 'language-switch-field';
      $element['value']['#attached']['library'][] = 'language_negotiator_content_entity_all_routes/language-widget-switch';
    }
  }
}
